-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 26, 2018 at 12:40 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.11-2+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stateCountry`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city`, `state_id`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'mumbai', 1, 7, '2018-12-25 05:17:42', '2018-12-25 05:17:42'),
(2, 'Nagpur', 1, 7, '2018-12-25 05:21:56', '2018-12-25 05:21:56'),
(3, 'Allahabad', 2, 7, '2018-12-25 05:22:50', '2018-12-25 05:22:50'),
(4, 'Aurangabad', 1, 7, '2018-12-25 07:03:04', '2018-12-25 07:03:04'),
(5, 'Bristol', 10, 11, '2018-12-25 07:22:16', '2018-12-25 07:22:16'),
(7, 'victoriaPalace', 8, 74, '2018-12-26 01:24:46', '2018-12-26 01:24:46');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country`, `created_at`, `updated_at`) VALUES
(7, 'India', '2018-12-20 08:31:35', '2018-12-20 08:31:35'),
(9, 'australia', '2018-12-20 08:33:25', '2018-12-20 08:33:25'),
(10, 'Russia', '2018-12-20 08:33:36', '2018-12-20 08:33:36'),
(11, 'USA', '2018-12-20 08:34:30', '2018-12-20 08:34:30'),
(12, 'Bangladesh', '2018-12-20 08:35:09', '2018-12-20 08:35:09'),
(15, 'ShriLanka', '2018-12-20 08:36:52', '2018-12-20 08:36:52'),
(16, 'Algeria', '2018-12-20 08:41:43', '2018-12-20 08:41:43'),
(17, 'Argentina', '2018-12-20 08:42:34', '2018-12-20 08:42:34'),
(18, 'Argentina', '2018-12-20 08:44:07', '2018-12-20 08:44:07'),
(19, 'Brazil', '2018-12-20 08:44:23', '2018-12-20 08:44:23'),
(20, 'Canada', '2018-12-20 08:44:52', '2018-12-20 08:44:52'),
(21, 'Canada', '2018-12-20 08:47:40', '2018-12-20 08:47:40'),
(22, 'Bermuda', '2018-12-20 08:47:58', '2018-12-20 08:47:58'),
(23, 'Colombia', '2018-12-20 08:48:28', '2018-12-20 08:48:28'),
(24, 'China', '2018-12-20 08:49:45', '2018-12-20 08:49:45'),
(25, 'China', '2018-12-20 08:56:48', '2018-12-20 08:56:48'),
(26, 'Finland', '2018-12-20 08:57:05', '2018-12-20 08:57:05'),
(27, 'Afganistan', '2018-12-21 00:32:25', '2018-12-21 00:32:25'),
(28, 'Astria', '2018-12-21 00:32:54', '2018-12-21 00:32:54'),
(29, 'Bulgeria', '2018-12-21 00:36:02', '2018-12-21 00:36:02'),
(30, 'Bhutan', '2018-12-21 00:40:12', '2018-12-21 00:40:12'),
(31, 'Burundi', '2018-12-21 00:41:16', '2018-12-21 00:41:16'),
(32, 'Croatia', '2018-12-21 00:41:47', '2018-12-21 00:41:47'),
(33, 'Cyprus', '2018-12-21 00:42:09', '2018-12-21 00:42:09'),
(35, 'Denmark', '2018-12-21 00:43:11', '2018-12-21 00:43:11'),
(36, 'Egypt', '2018-12-21 00:43:34', '2018-12-21 00:43:34'),
(41, 'France', '2018-12-21 00:57:52', '2018-12-21 00:57:52'),
(42, 'Iceland', '2018-12-21 00:59:28', '2018-12-21 00:59:28'),
(43, 'Indonesia', '2018-12-21 00:59:59', '2018-12-21 00:59:59'),
(44, 'Iran', '2018-12-21 01:01:19', '2018-12-21 01:01:19'),
(45, 'Iraq', '2018-12-21 01:01:40', '2018-12-21 01:01:40'),
(46, 'Israel', '2018-12-21 01:02:07', '2018-12-21 01:02:07'),
(47, 'Israel', '2018-12-21 01:05:29', '2018-12-21 01:05:29'),
(48, 'Zamiaca', '2018-12-21 01:05:53', '2018-12-21 01:05:53'),
(49, 'kuwait', '2018-12-21 01:06:11', '2018-12-21 01:06:11'),
(50, 'Kenya', '2018-12-21 01:08:50', '2018-12-21 01:08:50'),
(51, 'Libya', '2018-12-21 01:09:34', '2018-12-21 01:09:34'),
(52, 'Nepal', '2018-12-21 04:31:07', '2018-12-21 04:31:07'),
(53, 'Malavi', '2018-12-21 04:32:04', '2018-12-21 04:32:04'),
(54, 'Kazakhstan', '2018-12-21 04:32:50', '2018-12-21 04:32:50'),
(55, 'Zambia', '2018-12-21 04:33:47', '2018-12-21 04:33:47'),
(56, 'Netherlands', '2018-12-21 04:34:20', '2018-12-21 04:34:20'),
(72, 'India', '2018-12-21 07:26:33', '2018-12-21 07:26:33'),
(74, 'australia', '2018-12-21 08:44:19', '2018-12-21 08:44:19'),
(76, 'North korea', '2018-12-24 06:22:51', '2018-12-24 06:22:51');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_19_141939_create_details_table', 1),
(4, '2018_12_19_142402_create_states_table', 2),
(5, '2018_12_19_143126_create_cities_table', 3),
(6, '2018_12_19_143505_create_countries_table', 4),
(7, '2018_12_22_131023_add_country_to_states', 5),
(8, '2018_12_22_132040_add_country_to_states', 6),
(9, '2018_12_22_132135_add_country_to_states', 7),
(10, '2018_12_24_140851_add_country_id_to_state_table', 8),
(11, '2018_12_24_141124_add_state_id_to_state_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'Maharashtra', 7, '2018-12-22 08:16:59', '2018-12-22 08:16:59'),
(2, 'Uttar Pradesh', 7, '2018-12-22 09:00:23', '2018-12-22 09:00:23'),
(3, 'Rajasthan', 7, '2018-12-22 09:01:14', '2018-12-22 09:01:14'),
(4, 'Victoria', 9, '2018-12-22 09:05:08', '2018-12-22 09:05:08'),
(7, 'Ranchi', 15, '2018-12-24 00:12:54', '2018-12-24 00:12:54'),
(8, 'Disneyland', 74, '2018-12-24 01:40:54', '2018-12-24 01:40:54'),
(9, 'Newtown', 18, '2018-12-24 02:21:24', '2018-12-24 02:21:24'),
(10, 'California', 11, '2018-12-24 04:38:35', '2018-12-24 04:38:35'),
(11, 'coulmbia', 19, '2018-12-24 06:10:14', '2018-12-24 06:10:14'),
(12, 'Newyork', 41, '2018-12-24 06:11:29', '2018-12-24 06:11:29'),
(25, 'Chicago', 26, '2018-12-24 06:28:21', '2018-12-24 06:28:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

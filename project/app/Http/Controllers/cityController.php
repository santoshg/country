<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class cityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('countries')->select( 'id', 'country')->get();
//        $data = DB::table('states')->select( 'id', 'state')->get();
//        dd($data);
        return view('cityName', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = DB::table('cities')->select('id', 'city')->get();
//        dd($data);
        return Datatables::of($data)
            ->addIndexColumn()
            ->rawColumns(['action', 'delete'])
            ->addColumn('action', function($data){
                return '<a href="javascript:void();" onclick="editData('."'".Crypt::encryptString($data->id)."'".')">Edit</a>';
            })
            ->addColumn('delete', function($data){
                return '<a href="javascript:void();" onclick="deleteData('."'".Crypt::encryptString($data->id)."'".')">Delete</a>';
            })
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $data = new City();
        $data->city = $request->cityName;
        $data->state_id = $request->stateName;
        $data->country_id = $request->countryName;
        $data->save();
        return json_encode(['status'=>true, 'msg'=>'data inserted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['city_data'] = City::select('id', 'city', 'state_id', 'country_id')->where('id', Crypt::decryptString($id))->first();
        $id = ($data['city_data']['country_id']);
        $data['state_data'] = State::select('id', 'state')->where('country_id', $id)->get();
//        dd($data['state_data']);
        $data['country_data'] = Country::all();
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
                'city' => $request->input('cityName'),
                'state_id' => $request->input('stateName'),
                'country_id' => $request->input('countryName'),
        ];
        DB::table('cities')->where('id', Crypt::decryptString($id))->update($data);
        return json_encode(['status'=>true, 'msg'=>'data updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('cities')->where('id', Crypt::decryptString($id))->delete();
        return json_encode(['status'=>true, 'msg'=>'data deleted successfully']);
    }
    public function fetchState(Request $request){
        $dataState = DB::table('states')->where('country_id', $request->id)->get();
//        dd($dataState);
        return $dataState;
    }


}

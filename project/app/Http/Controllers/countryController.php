<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class countryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('countryName');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data =Country::select('id','country');
//        dd($data->get());
        return Datatables::of($data)
            ->addIndexColumn()
            ->rawColumns(['action', 'delete'])
            ->addColumn('action', function($data){
                return '<a href="javascript:void();" onclick="editData('."'".Crypt::encryptString($data->id)."'".')">Edit</a>';
//                return '<a href="#" onclick="editData()">Edit</a>';
            })
            ->addColumn('delete', function($data){
//                return '<a href="#" onclick="edit('."'".Crypt::encryptString($data->id)."'".')">Edit</a>';
                return '<a href="javascript:void();" onclick="deleteData('."'".Crypt::encryptString($data->id)."'".')">Delete</a>';
            })
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
//        $rules = [
//            'name'=>'required',
//        ];
//        validateUserRequest($request, $rules);

        $this->validate($request,[
            'countryName' => 'required',
        ]);
        $data = new Country();
        $data->country = $request->countryName;
        $data->save();
        return json_encode(['status'=>true, 'msg'=>'Data inserted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Country::select('country')->where('id', Crypt::decryptString($id))->first();
//        dd($data);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $data = [
        'country'=> $request->input('countryName'),
    ];
        DB::table('countries')->where('id', Crypt::decryptString($id))->update($data);
        return json_encode(["status"=>true, "msg"=>"data inserted successfully"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('countries')->where('id', Crypt::decryptString($id))->delete();
        return json_encode(["status"=>true, "msg"=>"data deleted successfylly"]);
    }
}

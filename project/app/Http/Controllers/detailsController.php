<?php

namespace App\Http\Controllers;


use App\Details;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class detailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('formValidation');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = new Details();
       $extension = Input::file('image')->getClientOriginalExtension();
       $fileName = 'img'.rand(11111, 99999). '.' .$extension;
//       $destinationPath = public_path('/images');
       $destinationPath = public_path('images');
//       dd($destinationPath);
       $request->file('image')->move($destinationPath, $fileName);
       $data->firstName = $request->firstName;
       $data->lastName= $request->lastName;
       $data->email= $request->email;
       $data->mobile= $request->number;
       $data->bday= $request->bday;
       $data->file= $request->image;
       $data->gender= $request->gender;
       $data->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $data['state'] = Details::select('id','state', 'country_id')->where('id', Crypt::decryptString($id))->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function numberValidation() {
        return view('numberValidation');
    }
    public function index1()
    {
        return view('middleware');
    }
    public function middleware1(){
        return ('Middleware were used successfully');
    }
}

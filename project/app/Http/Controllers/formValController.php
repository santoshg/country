<?php

namespace App\Http\Controllers;

use App\FormVal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class formValController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('formValidation');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = FormVal::select('id', 'firstName', 'lastName', 'email', 'mobile', 'bday', 'gender', 'file')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->rawColumns(['action', 'delete', 'file'])
            ->addColumn('action', function($data){
                return '<a href="javascript:void(0);" onclick="editDetails('."'".Crypt::encryptString($data->id)."'".')">Edit</a>';
            })
            ->addColumn('delete', function($data) {
                return '<a href="javascript:void(0);" onclick="deleteDetails(' . "'" . Crypt::encryptString($data->id) . "'" . ')">Delete</a>';
            })
            ->addColumn('file', function ($data) {
//                /*$pathInfo = pathinfo("/public/image/".$data->file);
//                $url = $pathInfo['dirname'].'/'.$pathInfo['filename'].'.'.$pathInfo['extension'];*/
////              dd(url($url));
                $orignalFile = url("project/public/images/".$data->file);
                return '<img src="'.$orignalFile.'"  height="50" width="50">';
            })
                ->make(true);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        dd($request);
        //$this -> form_validate($request); // call form_validate method to validate
        $data['error_string']=array();
        $data['inputerror']=array();
        $data['status']=TRUE;
        $rules=array(
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'number' => 'required|numeric',
            'bday' => 'required',
            'gender' =>'required|in:male,female',
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            $data['status']=FALSE;
            $message=$validator->messages();
            foreach ($rules as $key=>$value)
            {
                $data['error_string'][]=$message->first($key);
                $data['inputerror'][]=$key;
            }
            echo json_encode($data,true);
            exit;
            //return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = new FormVal();
        $extension = Input::file('image')->getClientOriginalExtension();
        $fileName = 'img'.rand(11111, 99999). '.' .$extension;
        $destinationPath = public_path('images');
        $request->file('image')->move($destinationPath, $fileName);
        $data->firstName = $request->firstName;
        $data->lastName= $request->lastName;
        $data->email= $request->email;
        $data->mobile= $request->number;
        $data->bday= $request->bday;
        $data->file= $fileName;
        $data->gender= $request->gender;
        $data->save();
        return json_encode(['status'=>true, 'msg'=>'data inserted successfully']);
    }

    public function form_validate($request){
        $data['error_string']=array();
        $data['inputerror']=array();
        $data['status']=TRUE;
        $rules=array(
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'number' => 'required|numeric',
            'bday' => 'required',
            'gender' =>'required|in:male,female',
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            $data['status']=FALSE;
            $message=$validator->messages();
            foreach ($rules as $key=>$value)
            {
                $data['error_string'][]=$message->first($key);
                $data['inputerror'][]=$key;
            }
            echo json_encode($data,true);
            exit;
            //return response()->json(['errors'=>$validator->errors()->all()]);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = FormVal::select('firstName', 'lastName', 'email', 'mobile', 'bday', 'gender', 'file')->where('id', Crypt::decryptString($id))->first();
//        dd($data->file);
        return $data;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return ($request);

//        dd($request->file);
        //$this -> form_validate($request);    //call validate method to validate the data

        $data['error_string']=array();
        $data['inputerror']=array();
        $data['status']=TRUE;
        $rules=array(
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'number' => 'required|numeric',
            'bday' => 'required',
            'gender' =>'required|in:male,female',
//            'image' => 'required',
//            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            $data['status']=FALSE;
            $message=$validator->messages();
            foreach ($rules as $key=>$value)
            {
                $data['error_string'][]=$message->first($key);
                $data['inputerror'][]=$key;
            }
            echo json_encode($data,true);
            exit;
            //return response()->json(['errors'=>$validator->errors()->all()]);
        }
        if($request->file != ''){
        $extension = Input::file('image')->getClientOriginalExtension();
            $fileName = 'img'.rand(11111, 99999). '.' .$extension;
            $destinationPath = public_path('images');
//        dd($destinationPath );
            $request->file('image')->move($destinationPath, $fileName);
            $data = [
                'firstName' => $request->input('firstName'),
                'lastName'=> $request->input('lastName'),
                'email'=> $request->input('email'),
                'mobile'=> $request->input('number'),
                'bday'=> $request->input('bday'),
                'gender'=> $request->input('gender'),
                'file'=> $fileName,
            ];
        }else{

            $data = [
                'firstName' => $request->input('firstName'),
                'lastName'=> $request->input('lastName'),
                'email'=> $request->input('email'),
                'mobile'=> $request->input('number'),
                'bday'=> $request->input('bday'),
                'gender'=> $request->input('gender'),
//                'file'=> $fileName,
            ];
        }
//        dd($extension);

        DB::table('form_vals')->where('id', Crypt::decryptString($id))->update($data);
        return response()->json(['status'=>true, 'msg'=>'data update successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//    dd($id);
        // DB::table('users')->delete($id);
        // The above is identical to this:
        DB::table('form_vals')->where('id', Crypt::decryptString($id))->delete();
        return json_encode(['status'=>true, 'msg'=>'data deleted successfully']);
    }
}

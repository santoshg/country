<?php

namespace App\Http\Controllers;

use App\Country;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class stateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $data = BD::table("countries")->select * FROM country
        $data = DB::table('countries')->select( 'id', 'country')->get();
//        dd($data[8]->country);
        return view('stateName', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = DB::table('states')->select('id', 'state')->get();
//        dd($data);
        return Datatables::of($data)
            ->addIndexColumn()
            ->rawColumns(['action', 'delete'])
            ->addColumn('action', function($data){
                return '<a href="javascript:void();" onclick="editData('."'".Crypt::encryptString($data->id)."'".')">Edit</a>';
            })
            ->addColumn('delete', function($data){
                return '<a href="javascript:void();" onclick="deleteData('."'".Crypt::encryptString($data->id)."'".')">Delete</a>';
            })
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $data = new State();
        $data->state = $request->stateName;
        $data->country_id = $request->countryName;
        $data->save();
        return json_encode(['status'=>true, 'msg'=>'Data inserted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['state'] = State::select('id','state', 'country_id')->where('id', Crypt::decryptString($id))->first();
        $data['country_data']= Country::all();
//        dd($data);
        return $data;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
            'state'=> $request->input('stateName'),
            'country_id'=> $request->input('countryName'),
        ];
        DB::table('states')->where('id', Crypt::decryptString($id))->update($data);
        return json_encode(["status"=>true, "msg"=>"data inserted successfully"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('states')->where('id', Crypt::decryptString($id))->delete();
        return json_encode(["status"=>true, "msg"=>"data deleted successfylly"]);
    }
}

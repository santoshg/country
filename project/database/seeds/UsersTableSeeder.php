<?php

use App\Http\Controllers\cityController;
use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->insert([
//            'name' => str_random(10),
//            'email' => str_random(10).'@gmail.com',
//            'email_verified_at' => now(),
//            'password' => bcrypt('secret'),
//        ]);
        factory(App\User::class, 10)->create()->each(function ($user) {
            $user->posts()->save(factory(cityController::class)->make());
        });
    }
}

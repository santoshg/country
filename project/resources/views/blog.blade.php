@extends('layouts.app')
@section('title', 'Blog')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('blog')
<h2>Blog</h2>
<input type="button" id="button" name="button" value="Add all details" class="btn btn-primary btn-lg" onclick="addDetails()">
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="detailsModal" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add details</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none"></div>
                    <form method="post" id="formDetails" class="form-group" enctype="multipart/form-data">
                        <input type="hidden" id="token" name="token" value="{{ csrf_token()}}">
                        <input type="hidden" id="save_method" name="save_method" value="">
                        <input type="hidden" id="form_id" name="form_id" value="">
                        <div class="form-group {{ $errors->has('firstName') ? 'has-error' : ''}}">
                            First Name:<br>
                            <input type="text" id="firstName" name="firstName" class="form-control" value="" placeholder="Enter your first name">
                            <span class="help-block">{{ $errors->first('firstName') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('lastName') ? 'has-error' : ''}}">
                            Last Name:<br>
                            <input type="text" id="lastName" name="lastName" class="form-control" value="" placeholder="Enter your last name">
                            <span class="help-block">{{ $errors->first('firstName') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('lastName') ? 'has-error' : ''}}">
                            E-mail Address:<br>
                            <input type="email" id="email" name="email" class="form-control" value="" placeholder="Enter your E-mail address">
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('number') ? 'has-error' : ''}}">
                            Mobile Number:<br>
                            <input type="text" id="number" name="number" class="form-control" value="" placeholder="Enter your Mobile Number">
                            <span class="help-block">{{ $errors->first('number') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('bday') ? 'has-error' : ''}}">
                            B-date:<br>
                            <input type="date" id="bday" name="bday" class="form-control" value="" placeholder="Enter your B-date">
                            <span class="help-block">{{ $errors->first('bday') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                            Gender:<br>
                            <input type="radio" id="gender_1" name="gender" value="male" > Male<br>
                            <input type="radio" id="gender_2" name="gender" value="female"> Female<br>
                            <input type="radio" id="gender_3" name="gender" value="other"> Other
                            <span class="help-block">{{ $errors->first('gender') }}</span>
                        </div><br>
                        <div id="img">
                            <img src="" class="image" style="width: 50px; height: 50px;">
                        </div>
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                            Image<br>
                            <input type="file" id="image" name="image" class="form-control" value="" placeholder="">
                            <span class="help-block">{{ $errors->first('file') }}</span>
                        </div><br>
                        <input type="button" id="submit" name="submit" class="form-control btn btn-primary" value="Submit">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <table class="table table-bordered" id="table" style="width: 90%;">
            <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email </th>
                <th>Phone No.</th>
                <th>B-date</th>
                <th>Gender</th>
                <th>Image</th>
                <th>Action</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>




@endsection
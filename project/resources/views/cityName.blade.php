@extends('layouts.app')
@section('title', 'City')
@section('city')
    <input type="button" id="button" name="button" value="Add City" class="btn btn-primary btn-lg" onclick="addCity()">
    <h1>All Cities</h1>
    <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add new Cities</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="cityForm" enctype="multipart/form-data" class="form-group" name="cityForm">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <input type="hidden" id="save_method" name="save_method" value="">
                            <input type="hidden" id="form_id" name="formName" value="">

                            <div class="form-group {{ $errors->has('cityName') ? 'has-error' : '' }} ">
                                City:<br>
                                <input type="text" class="form-control" id="cityName" name="cityName" value="" placeholder="Add New City">
                                <span class="help-block">{{ $errors->first('cityName') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('countryName') ? 'has-error' : '' }} ">
                                Country:<br>
                                <select class="form-control" name="countryName" id="countryName" value="country" onchange="checkState()">
                                    <option value="None" id="selectCountry" value="selectCountry">Select Country</option>
                                    @foreach($data as $value)

                                        <option value="{{$value->id}}">{{$value->country}}</option>

                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('countryName') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('countryName') ? 'has-error' : '' }} ">
                                State:<br>
                                <select class="form-control" name="stateName" id="stateName" value="state">
                                    <option value="None" id="selectState">Select State</option>

                                </select>
                                <span class="help-block">{{ $errors->first('stateName') }}</span>
                            </div>
                            <br>
                            <input type="button" id="submit" class="form-control btn btn-primary" value="Submit">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <table class="table table-bordered" id="table" style="width: 90%;">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>City Name</th>
                    <th>Action</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap.min.js"></script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // debugger;

        $(document).ready(function(){
            // debugger;
            // var table = $('#table');
            //To bind data to datatable
            $('#table').dataTable({
                responsive: true,
                "aaSorting": [],
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"] // change per page values here
                ],
                "pageLength": 25,
                //Ajax Request
                processing: true,
                serverSide: true,
                ajax: {
                    url : '{{ url('/cityName-details') }}',
                },
                columns: [
                    { data: 'id', name: 'id'},
                    { data: 'city', name : 'city' },
                    { data: 'action', name: 'action' },
                    { data: 'delete', name: 'delete' }
                ]
            });
        });


        function addCity(){
        debugger;
        saveMethod = "create";
        $('#myModal').modal('show');
        $('#cityForm')[0].reset();    //countryForm in form tag
        $('.form-group').removeClass('has-error');
        $('.alert alert-danger').removeClass('error');
        $('.help-block').empty();
        $('#save_method').val(saveMethod);
        $('.modal-title').html('Add New City');
        }




        function checkState() {
            var id = $('#countryName').val();
            // var id = document.getElementById("countryName").value;   // working too
            $.ajax({
               type: 'GET',
                url: '{{ url('/fetch-state') }}',
                data: {id:id},
                success:function (data) {
                    var ddlhtml='';
                    $.each(data, function(ind, val){
                       ddlhtml += '<option value="'+val.id+'">'+val.state+'</option>'
                    });
                    $('#stateName').html(ddlhtml);
                }
            });

        }

     $('#submit').click(function(){
        var cityName = $('#cityName').val();
        var stateName = $('#stateName').val();
        var countryName = $('#countryName').val();
        var id = $('#form_id').val();
        var status = 1;
         if(countryName == "None"){
             $('#countryName').parent().parent().addClass('has-error');
             $('#countryName').next().text('First addd country name');
             $('#countryName').focus();
             status = 0;
         }else{
             $('#countryName').parent().parent().removeClass('has-error');
             $('#countryName').next().text('');
         }
         if(stateName == "None"){
             $('#stateName').parent().parent().addClass('has-error');
             $('#stateName').next().text('First add state name');
             $('#stateName').focus();
             status = 0;
         }else{
             $('#stateName').parent().parent().removeClass('has-error');
             $('#stateName').next().text('');
         }
            if(cityName.length == 0){
                $('#cityName').parent().parent().addClass('has-error');
                $('#cityName').next().text('Please enter the city name');
                $('#cityName').focus();
            status = 0;
            }else{
                $('#cityName').parent().parent().removeClass('has-error');
                $('#cityName').next().text('');
            }
            if(status!=0 && saveMethod == 'create'){
            save();
            }else{
            updateData(id);
            }
        });

        function save(){
            var id = $('#form_id').val();
            var countryName = $('#countryName').val();
            var stateName = $('#stateName').val();
            var cityName = $('#cityName').val();
            var token = $('#token').val();
            var saveMethod = $('#save_method').val();
        $.ajax({
            type:'POST',
            url:'{{ url('/cityName') }}',
            dataType: 'json',
            data: {cityName: cityName, countryName:countryName, stateName:stateName, _token:token},
            success: function(data){
                if(data.status){
                $('#myModal').modal('hide');
                $('#table').DataTable().ajax.reload();
                }
            }
        });

        }

        function editData(id) {
            // debugger;
        saveMethod = "update";
        $('#cityForm')[0].reset();    //countryForm in form tag
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '{{ url('cityName-edit') }}' + '/' + id,
            success: function (data) {
                var ddlhtml ='';
                $.each(data.country_data, function(ind, val){
                   if(val.id == data.city_data.country_id){
                       ddlhtml += '<option value="'+val.id+'" selected>'+val.country +'</option>'
                   } else{
                       ddlhtml += '<option value="'+val.id +'">'+ val.country +'</option>'
                    }
                });
                var ddlhtml_state = '';
                $.each(data.state_data, function(ind, val){
                    if(val.id == data.city_data.state_id){
                        ddlhtml_state += '<option value="'+val.id+'" selected>'+val.state+'</option>'
                    }else{
                        ddlhtml_state += '<option value="'+val.id+'">'+val.state+'</option>'
                    }
                })
                $('#form_id').val(id);
                $('#cityName').val(data.city_data.city);
                $('#countryName').html(ddlhtml);
                $('#stateName').html(ddlhtml_state);
                $('.modal-title').html('Update data');
                $('#myModal').modal('show');
            }
        });

        }

        function updateData(id){
        // debugger;
            var status = 1;
            var cityName = $('#cityName').val();
            var stateName = $('#stateName').val();
            var countryName = $('#countryName').val();
            var token = $('#token').val();
            if(status!=0){
                status=1;
                }else{
                status=0;
                }
            $.ajax({
                type:'POST',
                url:'{{ url('/cityName-update') }}' + '/' + id,
                dataType:'json',
                data: {cityName:cityName, stateName:stateName, countryName:countryName, _token:token},
                success: function(data){
                    if(data.status){
                        $('#myModal').modal('hide');
                        $('#table').DataTable().ajax.reload();
                        }
                    }
                });
        }

        function deleteData(id) {
            if (confirm('Are you sure delete this tag?')) {
            var token = $('#token').val();
                $.ajax({
                    url: '{{ url('cityName-delete') }}' + '/' + id,
                    type: 'POST',
                    data: {_method: 'DELETE', _token: token},
                    dataType: "JSON",
                    success: function (data) {
                        $('#table').DataTable().ajax.reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error deleting data');
                        }
                });
            }
        }


        $('#cityForm').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
        e.preventDefault();
        return false;
        }
        });
    </script>
@endsection
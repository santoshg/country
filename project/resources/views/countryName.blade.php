@extends('layouts.app')
@section('title', 'Country')
@section('country')
<input type="button" id="button" name="button" value="Add Country" class="btn btn-primary btn-lg" onclick="addCountry()">
    <h1>All Countries</h1>
    <div class="container">
      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Countries</h4>
            </div>
            <div class="modal-body">
                <form method="POST" id="cityForm" enctype="multipart/form-data" class="form-group" name="cityForm" id="cityForm">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" id="save_method" name="save_method" value="">
                    <input type="hidden" id="form_id" name="formName" value="">

                    <div class="form-group {{ $errors->has('countryName') ? 'has-error' : '' }} ">
                    Country:<br>
                    <input type="text" class="form-control" id="countryName" name="countryName" value="" placeholder="Add New Country">
                        <span class="help-block">{{ $errors->first('countryName') }}</span>
                    </div>

                    <br>
                    <input type="button" id="submit" class="form-control btn btn-primary" value="Submit">
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

        <div class="container">
            <table class="table table-bordered" id="table" style="width: 90%;">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Country Name</th>
                    <th>Action</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

<script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // debugger;

        $(document).ready(function(){
            // debugger;
           // var table = $('#table');
            //To bind data to datatable
            $('#table').dataTable({
                responsive: true,
                "aaSorting": [],
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"] // change per page values here
                ],
                "pageLength": 25,
                //Ajax Request
                processing: true,
                serverSide: true,
                ajax: {
                    url : '{{ url('/countryName-details') }}',
                },
                columns: [
                    { data: 'id', name: 'id'},
                    { data: 'country', name : 'country' },
                    { data: 'action', name: 'action' },
                    { data: 'delete', name: 'delete' }
                ]
            });
        });



    function addCountry(){
        // debugger;
        saveMethod = "create";
     $('#myModal').modal('show');
     $('#cityForm')[0].reset();    //cityForm in form tag
     $('.form-group').removeClass('has-error');
     $('.help-block').empty();
     $('#save_method').val(saveMethod);
     $('.modal-title').html('Add New Country');
     $('#countryName').val();
    }


    $('#submit').click(function(){
       var countryName =$('#countryName').val();
       var id = $('#form_id').val();
       var status = 1;
       if(countryName.length == 0){
            $('#countryName').parent().parent().addClass('has-error');
            $('#countryName').next().text('Please enter the country name');
            $('#countryName').focus();
            status = 0;
       }else{
           $('#countryName').parent().parent().removeClass('has-error');
           $('#countryName').next().text('');
       }
        if(status!=0 && saveMethod == 'create'){
            save();
        }else{
            updateData(id);
        }
    });

    function save(){
        debugger;
        var status=1;
        var id = $('#form_id').val();
        var countryname = $('#countryName').val();
        var token = $('#_token').val();
        var saveMethod = $('#save_method').val();
        var form = $('#cityForm')[0];
        var formData = new FormData(form);
        if(status!=0){
            status=1;
        }else{
            status=0;
        }
        debugger;
        $.ajax({
        type:'POST',
        url:'{{ url('/countryName') }}',
        dataType:'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data){
            if(data.status){
                $('#myModal').modal('hide');
                $('#table').DataTable().ajax.reload();
                }
            }
        });

    }

        function editData(id) {
            saveMethod = "update";
            $('#cityForm')[0].reset();    //cityForm in form tag
            $('.form-group').removeClass('has-error');
            $('.help-block').empty();
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{ url('countryName-edit') }}' + '/' + id,
                success: function (data) {
                    $('#form_id').val(id);
                    $('#countryName').val(data.country);
                    $('.modal-title').html('Update data');
                    $('#myModal').modal('show');
                }
            });

        }

        function updateData(id){
        // debugger;
            var status = 1;
            var form = $('#cityForm')[0];
            var formData = new FormData(form);
            if(status!=0){
                status=1;
            }else{
                status=0;
            }
            $.ajax({
                type:'POST',
                url:'{{ url('/countryName-update') }}' + '/' + id,
                dataType:'json',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data){
                    if(data.status){
                        $('#myModal').modal('hide');
                        $('#table').DataTable().ajax.reload();
                    }
                }
            });
        }
        function deleteData(id) {
        debugger;
            if (confirm('Are you sure delete this tag?')) {
                var token = $('#token').val();
                $.ajax({
                    url: '{{ url('countryName-delete') }}' + '/' + id,
                    type: 'POST',
                    data: {_method: 'DELETE', _token: token},
                    dataType: "JSON",
                    success: function (data) {
                        $('#table').DataTable().ajax.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error deleting data');
                    }
                });
            }
        }


        $('#cityForm').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    </script>
@endsection
@extends('layouts.app')
@section('title', 'Form Validation')
<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('formValidation')

    <h2>Form Validation </h2>
    <input type="button" id="button" name="button" value="Add all details" class="btn btn-primary btn-lg" onclick="addDetails()">
    <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="detailsModal" role="dialog">
            <div class="modal-dialog">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger" style="display:none"></div>
                        <form method="post" id="formDetails" class="form-group" enctype="multipart/form-data">
                            <input type="hidden" id="token" name="token" value="{{ csrf_token()}}">
                            <input type="hidden" id="save_method" name="save_method" value="">
                            <input type="hidden" id="form_id" name="form_id" value="">
                            <div class="form-group {{ $errors->has('firstName') ? 'has-error' : ''}}">
                                First Name:<br>
                                <input type="text" id="firstName" name="firstName" class="form-control" value="" placeholder="Enter your first name">
                                <span class="help-block">{{ $errors->first('firstName') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('lastName') ? 'has-error' : ''}}">
                                Last Name:<br>
                                <input type="text" id="lastName" name="lastName" class="form-control" value="" placeholder="Enter your last name">
                                <span class="help-block">{{ $errors->first('firstName') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('lastName') ? 'has-error' : ''}}">
                                E-mail Address:<br>
                                <input type="email" id="email" name="email" class="form-control" value="" placeholder="Enter your E-mail address">
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('number') ? 'has-error' : ''}}">
                                Mobile Number:<br>
                                <input type="text" id="number" name="number" class="form-control" value="" placeholder="Enter your Mobile Number">
                                <span class="help-block">{{ $errors->first('number') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('bday') ? 'has-error' : ''}}">
                                B-date:<br>
                                <input type="date" id="bday" name="bday" class="form-control" value="" placeholder="Enter your B-date">
                                <span class="help-block">{{ $errors->first('bday') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                                Gender:<br>
                                <input type="radio" id="gender_1" name="gender" value="male" > Male<br>
                                <input type="radio" id="gender_2" name="gender" value="female"> Female<br>
                                <input type="radio" id="gender_3" name="gender" value="other"> Other
                                <span class="help-block">{{ $errors->first('gender') }}</span>
                            </div><br>
                            <div id="img">
                                <img src="" class="image" style="width: 50px; height: 50px;">
                            </div>
                            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                                Image<br>
                                <input type="file" id="image" name="image" class="form-control" value="" placeholder="" onclick="imgShow()">
                                <span class="help-block">{{ $errors->first('file') }}</span>
                            </div><br>
                            <input type="button" id="submit" name="submit" class="form-control btn btn-primary" value="Submit">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <table class="table table-bordered" id="table" style="width: 90%;">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email </th>
                    <th>Phone No.</th>
                    <th>B-date</th>
                    <th>Gender</th>
                    <th>Image</th>
                    <th>Action</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap.min.js"></script>
    {{--<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>--}}
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function(){
            $('#table').dataTable({
                responsive: true,
                "aaSorting": [],
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"] // change per page values here
                ],
                "pageLength": 25,
                //Ajax Request
                processing: true,
                serverSide: true,
                ajax: {
                    url : '{{ url('/formValidation-details') }}',
                },
                columns: [
                    { data: 'id', name: 'id', orderable: false,},
                    { data: 'firstName', name : 'firstName' },
                    { data: 'lastName', name : 'lastName' },
                    { data: 'email', name : 'email' },
                    { data: 'mobile', name : 'mobile' },
                    { data: 'bday', name : 'bday' },
                    { data: 'gender', name : 'gender' },
                    { data: 'file', name : 'file' },
                    { data: 'action', name: 'action' },
                    { data: 'delete', name: 'delete'}
                ]
            });
        });

        function addDetails(){
            saveMethod = "create";
            $('#detailsModal').modal('show');
            $('#formDetails')[0].reset();
            $('.form-group').removeClass('has-error');
            $('.alert alert-danger').removeClass('error');
            $('.help-block').empty();
            $('.modal-title').html('Add Details');
            $('#save_method').val(saveMethod);
            $('#img').hide();
        }

        // function imgShow(){
        //     $('#img').show();
        // }

        //preventing character in input number field
        $('#number').keypress(function(key) {
            if(key.charCode < 48 || key.charCode > 57)
                return false;
        });

        function imgCheck(){
            debugger;
            var image = $('#image').val();
            if(image.length == 0){
                $('#image').parent().addClass('has-error');
                $('#image').next().text('Choose the file');
                $('#image').focus();
                status = 0;
            }else{
                $('#image').parent().removeClass('has-error');
                $('#image').next().text('');
                status =1;
            }

            return status;
        }

        $('#submit').click(function(){
            debugger;
            var id = $('#form_id').val();
            var firstName = $('#firstName').val();
            var lastName = $('#lastName').val();
            var email = $('#email').val();
            var number = $('#number').val();
            var bday = $('#bday').val();
            var gender = $('input[name="gender"]:checked').val();
            // var saveMethod = $('#save_method').val();
            var _token = $('#token').val();
            var name_regex = /^[a-zA-Z]+$/;
            var email_regex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/;
            var status = 1;
            // if(gender == undefined){
            //     $('#gender').parent().addClass('has-error');
            //     $('#gender').next().next().next().next().next().text('Select gender');
            //     status = 0;
            // }else{
            //     $('#gender').parent().removeClass('has-error');
            //     $('#gender').next().next().next().next().next().text('');
            // }
            if(bday.length == 0){
                $('#bday').parent().addClass('has-error');
                $('#bday').next().text('First add B-date');
                $('#bday').focus();
                status = 0;
            }else{
                $('#bday').parent().removeClass('has-error');
                $('#bday').next().text('');
            }
            if(number.length == 0){
                $('#number').parent().addClass('has-error');
                $('#number').next().text('Add mobile number');
                $('#number').focus();
                status = 0;
            }else if(number.length != 10){
                $('#number').parent().addClass('has-error');
                $('#number').next().text('First proper number');
                $('#number').focus();
                status = 0;
            }else{
                $('#number').parent().removeClass('has-error');
                $('#number').next().text('');
            }
            if(email.length == 0){
                $('#email').parent().addClass('has-error');
                $('#email').next().text('First add email address');
                $('#email').focus();
                status = 0;
            }/*else if(!email.match(email_regex)){
                $('#email').parent().parent().addClass('has-error');
                $('#email').next().text('Enter proper email address');
                $('#email').focus();
                status = 0;
            }*/else{
                $('#email').parent().removeClass('has-error');
                $('#email').next().text('');
            }
            if(lastName.length == 0){
                $('#lastName').parent().addClass('has-error');
                $('#lastName').next().text('First add last name');
                $('#lastName').focus();
                status = 0;
            }
            else if(!lastName.match(name_regex)){
                $('#lastName').parent().addClass('has-error');
                $('#lastName').next().text('Enter only alphabetical value');
                $('#lastName').focus();
                status = 0;
            }else{
                $('#lastName').parent().removeClass('has-error');
                $('#lastName').next().text('');
            }
            if(firstName.length == 0){
                $('#firstName').parent().addClass('has-error');
                $('#firstName').next().text('First add name');
                $('#firstName').focus();
                status = 0;
            }else if(!firstName.match(name_regex)){
                $('#firstName').parent().addClass('has-error');
                $('#firstName').next().text('Enter only alphabetical value ');
                $('#firstName').focus();
                status = 0;
            }else{
                $('#firstName').parent().removeClass('has-error');
                $('#firstName').next().text('');
            }
            if(saveMethod == 'create'){
                imgCheck()
            }else{}
            if(status !=0 && saveMethod == 'create'){
                save();
            }else{
                update(id);
            }
        });

        function save(){
            debugger;
            //console.log($('#formDetails').serialize());
            var saveMethod = $('#save_method');
            var formDetails = $('#formDetails')[0];
            imgCheck();
            var data = new FormData(formDetails);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
               type: 'POST',
                url: '{{ url('/formValidation-store') }}',
                data: data,
                cache:false,
                async:false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success:function (data){
                    debugger;
                 if(data.errors){
                        for (var i = 0; i < data.inputerror.length; i++) {
                            if (data.error_string[i] != '') {
                                $('#' + data.inputerror[i] + '').parent().addClass('has-error');
                                $('#' + data.inputerror[i] + '').next().text('' + data.error_string[i] + '');

                            } else {
                                $('#' + data.inputerror[i] + '').parent().removeClass('has-error');
                                $('#' + data.inputerror[i] + '').next().text('');
                            }
                        }
                    }
                    else{
                        $('#detailsModal').modal('hide');
                        $('#table').DataTable().ajax.reload();
                    }

                },

            });
        }

        function editDetails(id){
            saveMethod = "update";
            $('#formDetails')[0].reset();
            $('.form-group').removeClass('has-error');
            $('.help-block').empty();
            $.ajax({
                type: 'GET',
                url: '{{ url('/formValidation-edit') }}' + '/' + id,
                dataType: 'json',
                success:function (data) {
                    var male ="male";
                    var female = "female";
                    $('#form_id').val(id);
                    $('#firstName').val(data.firstName);
                    $('#lastName').val(data.lastName);
                    $('#email').val(data.email);
                    $('#number').val(data.mobile);
                    $('#bday').val(data.bday);
                    $('.image').attr('src','project/public/images/'+ data.file);

                    // $('#image').val(data.file);
                    $('.modal-title').html('Update data');
                    $('#detailsModal').modal('show');
                    $('#img').show();
                    var gender = data.gender;
                    if(gender == "male") {
                        $("#gender_1").prop("checked", true);
                    } else if(gender == "female"){
                        $("#gender_2").prop("checked", true);
                    } else if(data.gender == "other"){
                        $("#gender_3").prop("checked", true);
                    }
                }

            });
        }

        function update(id){
            debugger;
            var formDetails = $('#formDetails')[0];
            var data = new FormData(formDetails);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '{{ url('/formValidation-update') }}' + '/' + id,
                data: data,
                cache:false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success:function (data){
                    if(data.status){
                        $('#detailsModal').modal('hide');
                        $('#table').DataTable().ajax.reload();
                    }
                }
            });
        }
        function deleteDetails(id){
            if (confirm('Are you sure delete this tag?')) {
                var token = $('#token').val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url('formValidation-delete') }}' + '/' + id,
                    type: 'POST',
                    data: {_method: 'DELETE', _token: token},
                    dataType: "JSON",
                    success: function (data) {
                        $('#table').DataTable().ajax.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error deleting data');
                    }
                });
            }
        }

        /*$("#submit").validate({
            debug: true
        });
        $("#myform").validate({
            rules: {
                // simple rule, converted to {required:true}
                name: "required",
                // compound rule
                email: {
                    required: true,
                    email: true
                }
            }
        });
        $$("#myform").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: "Please specify your name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                }
            }
        });*/
/*$('#submit').ready(function(){
    alert(123);
    debugger;
        $("#formDetails").validate({

            rules: {
                firstName: {
                    required: true,
                    minlength: 2
                },
                lastName:{
                    required: true,
                    minlength: 2
                },
                email:{
                  required: true,
                },
                number:{
                    required: true,
                    minlength: 10
                },
            },
            messages: {
                firstName: {
                    required: "We need your name to contact you",
                    minlength: jQuery.validator.format("At least {0} characters required!")
                },
                lastName: {
                    required: "We need your last name too to contact you",
                    minlength: jQuery.validator.format("At least {0} characters required!")
                },
                email: {
                    required: "We need your email address to contact you",
                    minlength: jQuery.validator.format("At least {0} characters required!")
                },
                number: {
                    required: "We need your contact number to contact you",
                    minlength: jQuery.validator.format("At least {0} characters required!")
                }
            }
        });
});*/
    </script>
@endsection
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src ="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Validations-@yield('title')</title>
    </head>
    {{--script to cleare data of modal on opeing--}}
    {{--<script>--}}
    {{--$('#myModal').on('hidden.bs.modal', function (e) {--}}
    {{--$('#countryName').val(' ');--}}
    {{--});--}}
    {{--</script>--}}
</head>
    <style>
        body {
            font-family: "Lato", sans-serif;
        }

        .sidenav {
            height: 100%;
            width: 200px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #111;
            overflow-x: hidden;
            padding-top: 20px;
        }

        .sidenav a {
            padding: 6px 6px 6px 32px;
            text-decoration: none;
            font-size: 25px;
            color: #818181;
            display: block;
        }

        .sidenav a:hover {
            color: #f1f1f1;
        }

        .main {
            margin-left: 200px; /* Same as the width of the sidenav */
        }

        @media screen and (max-height: 450px) {
            .sidenav {padding-top: 15px;}
            .sidenav a {font-size: 18px;}
        }
    </style>
</head>
<body>

<div class="sidenav">
    <a href="{{ url('/') }}">Dashboard</a>
    <a href="{{ url('/countryName') }}">Country</a>
    <a href="{{ url('/stateName') }}">State</a>
    <a href="{{ url('/cityName') }}">City</a>
    <a href="{{ url('/numberValidation') }}"><h3>Phone Number Validation</h3></a>
    <a href="{{ url('/formValidation') }}"><h3>Form Validation</h3></a>
    <a href="{{ url('/sendMail') }}"><h3>Mail Function</h3></a>
    {{--<a href="{{ url('/middleware') }}"><h3>Middleware</h3></a>--}}
    <a href="{{ url('/blog') }}"><h3>Blog</h3></a>

</div>
<div class="main" style="padding-top:50px; padding-left:30px;">
    @yield('/')

    @yield('country')

    @yield('state')

    @yield('city')

    @yield('phone')

    @yield('formValidation')

    @yield('mailSection')

    @yield('middleware')

    @yield('blog')
</div>




</body>
</html>

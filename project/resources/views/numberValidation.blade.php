@extends('layouts.app')
@section('title', 'Number validation')
@section('phone')
    <h1>Phone Numer Validation</h1>
    <div id="mobile">
        <div class="input">
            <div class="row">
                <div class="col-xs-9">
                    Mobile No:<input id="mobile-num" type="text" class="form-control form-change-element" />
                </div>
            </div>
        </div>
    </div>
<script>

    $(document).ready(function(){

        $("#mobile-num").on("blur", function(){
        var mobNum = $(this).val();
        var filter = /^\d*(?:\.\d{1,2})?$/;

            if (filter.test(mobNum)) {
                if(mobNum.length==10){
                    alert("valid");
                    } else {
                    alert('Please put 10  digit mobile number');

                    return false;
                }
            }
            else {
                alert('Not a valid number');
                return false;
            }

         });

    });

    $('#mobile-num').keypress(function(key) {
    if(key.charCode < 48 || key.charCode > 57)
        return false;
    });
    </script>
@endsection
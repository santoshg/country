@extends('layouts.app')
@section('title', 'State')
@section('country')
    <input type="button" id="button" name="button" value="Add State" class="btn btn-primary btn-lg" onclick="addState()">
    <h1>All States</h1>
    <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">States</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" id="stateForm" enctype="multipart/form-data" class="form-group" name="stateForm" id="stateForm">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <input type="hidden" id="save_method" name="save_method" value="">
                            <input type="hidden" id="form_id" name="formName" value="">

                            <div class="form-group {{ $errors->has('stateName') ? 'has-error' : '' }} ">
                                State Name:<br>
                                <input type="text" class="form-control" id="stateName" name="stateName" value="" placeholder="Add New State">
                                <span class="help-block">{{ $errors->first('stateName') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('countryName') ? 'has-error' : '' }} ">
                                Country:<br>
                                <select class="form-control" name="countryName" id="countryName" value="country" placeholder="Select the country name" >
                                    <option value="None">-- Select --</option>
                                    @foreach($data as $value)

                                        <option value="{{$value->id}}">{{$value->country}}</option>

                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('countryName') }}</span>
                            </div>

                            <br>
                            <input type="button" id="submit" class="form-control btn btn-primary" value="Submit">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <table class="table table-bordered" id="table" style="width: 90%;">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>State Name</th>
                    <th>Action</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // debugger;

        $(document).ready(function(){
            // debugger;
            // var table = $('#table');
            //To bind data to datatable
            $('#table').dataTable({
                responsive: true,
                "aaSorting": [],
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"] // change per page values here
                ],
                "pageLength": 25,
                //Ajax Request
                processing: true,
                serverSide: true,
                ajax: {
                    url : '{{ url('/stateName-details') }}',
                },
                columns: [
                    { data: 'id', name: 'id'},
                    { data: 'state', name : 'state' },
                    { data: 'action', name: 'action' },
                    { data: 'delete', name: 'delete' }
                ]
            });
        });




        function addState(){
            // debugger;
            saveMethod = "create";
            $('#myModal').modal('show');
            $('#stateForm')[0].reset();    //stateForm in form tag
            $('.form-group').removeClass('has-error');
            $('.help-block').empty();
            $('#save_method').val(saveMethod);
            $('.modal-title').html('Add New State');
            $('#stateName').val();
            $('#countryName').val();
        }


        $('#submit').click(function(){
            // debugger;
            var stateName =$('#stateName').val();
            var countryName = $('#countryName').val();
            var id = $('#form_id').val();
            var status = 1;
            if(countryName.length == 0){
                $('#countryName').parent().parent().addClass('has-error');
                $('#countryName').next().text('Please select country name');
                $('#countryName').focus();
            }else{
                $('#countryName').parent().parent().removeClass('has-error');
                $('#countryName').next().text('');
            }
            if(stateName.length == 0){
                $('#stateName').parent().parent().addClass('has-error');
                $('#stateName').next().text('Please enter the state name');
                $('#stateName').focus();
                status = 0;
            }else{
                $('#stateName').parent().parent().removeClass('has-error');
                $('#stateName').next().text('');
            }

            if(status!=0 && saveMethod == 'create'){
                save();
            }else{
                updateData(id);
            }
        });

        function save(){
            // debugger;
            var status=1;
            var id = $('#form_id').val();
            var stateName = $('#stateName').val();
            var CountryName = $('#CountryName').val();
            var token = $('#_token').val();
            var saveMethod = $('#save_method').val();
            var form = $('#stateForm')[0];
            var formData = new FormData(form);
            if(status!=0){
                status=1;
            }else{
                status=0;
            }
            // debugger;
            $.ajax({
                type:'POST',
                url:'{{ url('/stateName') }}',
                dataType:'json',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data){
                    if(data.status){
                        $('#myModal').modal('hide');
                        $('#table').DataTable().ajax.reload();
                    }
                }
            });

        }

        function editData(id) {
            // debugger;
            saveMethod = "update";
            $('#stateForm')[0].reset();    //stateForm in form tag
            $('.form-group').removeClass('has-error');
            $('.help-block').empty();
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{ url('stateName-edit') }}' + '/' + id,
                success: function (data) {
                    // debugger;
                    var ddlhtml='';
                    $.each(data.country_data,function(ind,val){
                       if(val.id == data.state.country_id) {
                            ddlhtml += '<option value="'+val.id+'"selected >'+val.country+'</option>'
                       } else {
                           ddlhtml += '<option value="'+val.id+'">'+val.country+'</option>'
                       }
                    });
                    $('#form_id').val(id);
                    $('#stateName').val(data.state.state);
                    $('#countryName').html(ddlhtml);
                    $('.modal-title').html('Update data');
                    $('#myModal').modal('show');
                }
            });

        }

        function updateData(id){
            // debugger;
            var status = 1;
            var form = $('#stateForm')[0];
            var formData = new FormData(form);
            if(status!=0){
                status=1;
            }else{
                status=0;
            }
            $.ajax({
                type:'POST',
                url:'{{ url('/stateName-update') }}' + '/' + id,
                dataType:'json',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data){
                    if(data.status){
                        $('#myModal').modal('hide');
                        $('#table').DataTable().ajax.reload();
                    }
                }
            });
        }
        function deleteData(id) {
            if (confirm('Are you sure delete this tag?')) {
                var token = $('#token').val();
                $.ajax({
                    url: '{{ url('stateName-delete') }}' + '/' + id,
                    type: 'POST',
                    data: {_method: 'DELETE', _token: token},
                    dataType: "JSON",
                    success: function (data) {
                        $('#table').DataTable().ajax.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error deleting data');
                    }
                });
            }
        }

        $('#stateForm').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    </script>
@endsection
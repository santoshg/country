<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//countries route
Route::get('/countryName', 'countryController@index');
Route::post('/countryName', 'countryController@store');
Route::get('/countryName-details', 'countryController@create');
Route::get('/countryName-edit/{id}', 'countryController@edit');
Route::post('/countryName-update/{id}', 'countryController@update');
Route::delete('/countryName-delete/{id}', 'countryController@destroy');
//states route
Route::get('/stateName', 'stateController@index');
Route::post('/stateName', 'stateController@store');
Route::get('/stateName-details', 'stateController@create');
Route::get('/stateName-edit/{id}', 'stateController@edit');
Route::post('/stateName-update/{id}', 'stateController@update');
Route::delete('/stateName-delete/{id}', 'stateController@destroy');
//cities route
Route::get('/cityName', 'cityController@index');
Route::get('/fetch-state', 'cityController@fetchState');
Route::post('/cityName', 'cityController@store');
Route::get('/cityName-details', 'cityController@create');
Route::get('/cityName-edit/{id}', 'cityController@edit');
Route::post('/cityName-update/{id}', 'cityController@update');
Route::delete('/cityName-delete/{id}', 'cityController@destroy');
//number validation
Route::get('/numberValidation', 'detailsController@numberValidation');
//formValidation routes
Route::get('/formValidation', 'formValController@index');
Route::get('/formValidation-details', 'formValController@create');
Route::post('/formValidation-store', 'formValController@store');
Route::get('/formValidation-edit/{id}', 'formValController@edit');
Route::post('/formValidation-update/{id}', 'formValController@update');
Route::delete('/formValidation-delete/{id}', 'formValController@destroy');
//routes for mail function
Route::get('/sendMail', 'mailController@index');
Route::post('/sendMailtoG', 'mailController@mail');
//using middleware
Route::get('/middleware', 'detailsController@index1');
Route::post('/middleware', 'detailsController@middleware1')->middleware('calculate');

Route::get('session/get','SessionController@accessSessionData');
Route::get('session/set','SessionController@storeSessionData');
Route::get('session/remove','SessionController@deleteSessionData');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//for blog
Route::get('blog', 'blogController@index');
